package com.example.activity;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import android.app.Application;

public class MyAppliction extends Application {
	public static RequestQueue queues;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		queues=Volley.newRequestQueue(getApplicationContext());
	}
	
	public static RequestQueue getHttpQueues() {
		return queues;
	}
}
