package com.example.activity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import com.baidu.mapapi.SDKInitializer;
import com.example.aaageizistopcar.R;
import com.example.myutils.SlidingMenu;

public class MainActivity extends Activity implements OnClickListener
{

	private SlidingMenu slidingMenu;
	RelativeLayout silder_menu_1;
	RelativeLayout silder_menu_2;
	
	public Fragment fragment_mainmap, fragment_second;
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		SDKInitializer.initialize(getApplicationContext());
		setContentView(R.layout.activity_main);

		slidingMenu = (SlidingMenu) findViewById(R.id.id_menu);
		
		silder_menu_1=(RelativeLayout) findViewById(R.id.silder_menu_1);
		silder_menu_2=(RelativeLayout) findViewById(R.id.silder_menu_2);
		silder_menu_1.setOnClickListener(this);
		silder_menu_2.setOnClickListener(this);
		
		selectItem(1);
		
	}
	
	public void toggleMenu(View view)
	{
		slidingMenu.toggle();
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.silder_menu_1:
			selectItem(1);
			break;
		case R.id.silder_menu_2:
			selectItem(2);
			break;

		default:
			break;
		}
		
	}
	
	public void selectItem(int i)
	{
		FragmentManager fm = getFragmentManager();
		FragmentTransaction transaction = fm.beginTransaction();
		hideFragment(transaction);
		
		switch (i)
		{
		case 1:
			if (fragment_mainmap == null)
			{
				fragment_mainmap = new MainMapFragement(); 
				transaction.add(R.id.id_content, fragment_mainmap);
			} 
			else
			{
				transaction.show(fragment_mainmap);
			}
			break;
		case 2:
			if (fragment_second == null)
			{
				fragment_second = new SecondFragement();
				transaction.add(R.id.id_content, fragment_second);
			} 
			else
			{
				transaction.show(fragment_second);
			}
			break;

		default:
				break;
		}
		transaction.commit();
	}
	
	private void hideFragment(FragmentTransaction transaction)
	{
		if (fragment_mainmap != null)
		{
			transaction.hide(fragment_mainmap);
		}
		if (fragment_second!= null)
		{
			transaction.hide(fragment_second);
		}
	}

}