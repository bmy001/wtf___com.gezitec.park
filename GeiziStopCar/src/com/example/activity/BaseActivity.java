package com.example.activity;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.example.aaageizistopcar.R;
import com.example.network.VolleyInterface;
import com.example.network.VolleyRequest;

public class BaseActivity extends Activity
{
	
	public void setTitle(String titleString)
	{
		ImageButton btn_title_back = (ImageButton) findViewById(R.id.btn_title_back);
		TextView tv_title_name = (TextView) findViewById(R.id.tv_title_name);
		btn_title_back.setOnClickListener(new View.OnClickListener()
		{

			@Override
			public void onClick(View arg0)
			{
				// TODO Auto-generated method stub
				finish();
			}
		});
		tv_title_name.setText(titleString);
	}
	
	public void saveSharePreference(String name,String value)
	{
		SharedPreferences.Editor editor =getSharedPreferences("search_condition", MODE_PRIVATE).edit();
		editor.putString(name, value);
		editor.commit();
	
	}
	public String readSharePreference(String name)
	{
		SharedPreferences pref=getSharedPreferences("search_condition", MODE_PRIVATE);
		String value=pref.getString(name, "");
		
		return value;
	}
	
	/*
	private void volleyPost()
	{
		String url = "http://193.193.193.242:6868/dcapi/api/app/gys/login";
		String tag = "abc";
		Map<String, String> params = new HashMap<String, String>();
		params.put("userName", "144203923");
		params.put("userPass", "hgDMmSTsM44=");
		VolleyRequest.RequestPost(this, Method.POST, url, tag, params, new VolleyInterface(this, VolleyInterface.mListener, VolleyInterface.mErrorListtener)
		{

			@Override
			public void onMySuccess(String result)
			{
				System.out.println("**" + result);

			}

			@Override
			public void onMyError(VolleyError error)
			{
				// TODO Auto-generated method stub

			}
		});

	}

	private void volleyGet()
	{
		String url = "";
		String tag = "abc";
		VolleyRequest.RequestGet(this, url, tag, new VolleyInterface(this, VolleyInterface.mListener, VolleyInterface.mErrorListtener)
		{

			@Override
			public void onMySuccess(String result)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void onMyError(VolleyError error)
			{
				// TODO Auto-generated method stub

			}
		});
	}

	// volley和activity的关联
	@Override
	protected void onStop()
	{
		// TODO Auto-generated method stub
		super.onStop();
		// 在停止的时候也把tag标签的网络请求给停掉
		MyAppliction.getHttpQueues().cancelAll("");
	}
	*/
}
