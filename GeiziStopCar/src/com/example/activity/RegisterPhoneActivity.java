package com.example.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.Request.Method;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.aaageizistopcar.R;
import com.example.logic.ServerAddr;
import com.example.network.VolleyInterface;
import com.example.network.VolleyRequest;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterPhoneActivity extends BaseActivity implements OnClickListener
{
	EditText et_phone_num;
	Button btn_registerphonenum_next;
	
	String phone_num="";
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_phone);
		setTitle("ע���ֻ���");
		initView();
		setListenerEvent();
	}

	private void setListenerEvent()
	{
		btn_registerphonenum_next.setOnClickListener(this);
		
	}

	private void initView()
	{
		et_phone_num=(EditText) findViewById(R.id.et_phone_num);
		btn_registerphonenum_next=(Button) findViewById(R.id.btn_registerphonenum_next);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.btn_registerphonenum_next:
//			System.out.println("R.id.btn_registerphonenum_next");
			
			phone_num=et_phone_num.getText().toString().trim();
			sendPhoneGetCheckCodeToServer();
			
//			Intent intent=new Intent(RegisterPhoneActivity.this, RegisterPwdActivity.class);
//			startActivity(intent);
			
//			finish();
			
			break;

		default:
			break;
		}
	}
	
	
	
	
	private void sendPhoneGetCheckCodeToServer()
	{
		String url = ServerAddr.SEND_PHONENUM;
		String tag = "abc";
		final Map<String, String> params = new HashMap<String, String>();
		params.put("PhoneNumber", phone_num);
		VolleyRequest.RequestPost(this, Method.POST, url, tag, params, new VolleyInterface(this, VolleyInterface.mListener, VolleyInterface.mErrorListtener)
		{

			@Override
			public void onMySuccess(String result)
			{
				
				System.out.println("Success**" + result);
				
				JSONObject jsonObject;
				String code="";
				try
				{
					jsonObject = new JSONObject(result);
					code = jsonObject.getString("code");
				} catch (JSONException e)
				{
					e.printStackTrace();
				}
				if (code.equals("900"))
				{
					System.out.println("Success**" + result);
					
					Intent intent=new Intent(RegisterPhoneActivity.this, RegisterPwdActivity.class);
					startActivity(intent);
					saveSharePreference("PhoneNumber", phone_num);
					
					finish();
				}
				else 
				{
					Toast.makeText(getApplicationContext(), "result="+result, 0).show();
				}
				
				
				
			}

			@Override
			public void onMyError(VolleyError error)
			{
				System.out.println("error**" + error);
//				Toast.makeText(getApplicationContext(), "������", 0).show();
			}
		});

	}
}
