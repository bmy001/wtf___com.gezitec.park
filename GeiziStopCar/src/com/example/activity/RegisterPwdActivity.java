package com.example.activity;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.VolleyError;
import com.android.volley.Request.Method;
import com.example.aaageizistopcar.R;
import com.example.logic.ServerAddr;
import com.example.network.VolleyInterface;
import com.example.network.VolleyRequest;

import android.R.integer;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegisterPwdActivity extends BaseActivity implements OnClickListener
{
	int TotalTime=60;
	
	EditText et_pwd_num;
	EditText et_checkcode;
	
	Button btn_registerpwd_ok;
	Button btn_getcheckcode;
	
	String pwd_num_str;
	String checkcode_str;
	
	Handler handler=new Handler()
	{
		public void handleMessage(android.os.Message msg) 
		{
			if (msg.what>0)
			{
				btn_getcheckcode.setText("ʣ��"+msg.what+"��");
				msg.what--;
				
				sendEmptyMessageDelayed(msg.what, 1000);
			}
			else 
			{
				btn_getcheckcode.setText("��ȡ��֤��");
				btn_getcheckcode.setClickable(true);
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register_pwd);
		setTitle("ע������");
		
		initView();
		setListenerEvent();
		
		//��ȡ��֤��ļ�ʱ
		handler.sendEmptyMessage(TotalTime);
		btn_getcheckcode.setClickable(false);
	}

	private void setListenerEvent()
	{
		btn_registerpwd_ok.setOnClickListener(this);
		btn_getcheckcode.setOnClickListener(this);
		
	}

	private void initView()
	{
		et_pwd_num=(EditText) findViewById(R.id.et_pwd_num);
		btn_registerpwd_ok=(Button) findViewById(R.id.btn_registerpwd_ok);
		
		et_checkcode=(EditText) findViewById(R.id.et_checkcode);
		btn_getcheckcode=(Button) findViewById(R.id.btn_getcheckcode);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.btn_registerpwd_ok:

			pwd_num_str=et_pwd_num.getText().toString().trim();
			checkcode_str=et_checkcode.getText().toString().trim();
			
			RegisterUserToServer();
			
//			finish();
			
			break;
		case R.id.btn_getcheckcode:
			
//			handler.sendEmptyMessage(TotalTime);
//			btn_getcheckcode.setClickable(false);
			
			sendPhoneGetCheckCodeToServer();
			
			break;

		default:
			break;
		}
	}
	
	private void sendPhoneGetCheckCodeToServer()
	{
		String url = ServerAddr.SEND_PHONENUM;
		String tag = "abc";
		final Map<String, String> params = new HashMap<String, String>();
		params.put("PhoneNumber", readSharePreference("PhoneNumber"));
		VolleyRequest.RequestPost(this, Method.POST, url, tag, params, new VolleyInterface(this, VolleyInterface.mListener, VolleyInterface.mErrorListtener)
		{

			@Override
			public void onMySuccess(String result)
			{
				
				System.out.println("Success**" + result);
				
				JSONObject jsonObject;
				String code="";
				try
				{
					jsonObject = new JSONObject(result);
					code = jsonObject.getString("code");
				} catch (JSONException e)
				{
					e.printStackTrace();
				}
				if (code.equals("900"))
				{
					System.out.println("Success**" + result);
					
					handler.sendEmptyMessage(TotalTime);
					btn_getcheckcode.setClickable(false);
				}
				else 
				{
					Toast.makeText(getApplicationContext(), "result="+result, 0).show();
				}
				
				
				
			}

			@Override
			public void onMyError(VolleyError error)
			{
				System.out.println("error**" + error);
				Toast.makeText(getApplicationContext(), "��������ʧ��   ������", 0).show();
			}
		});

	}
	
	private void RegisterUserToServer()
	{
		String url = ServerAddr.SEND_PHONENUM;
		String tag = "abc";
		final Map<String, String> params = new HashMap<String, String>();
		
		params.put("PhoneNumber", readSharePreference("PhoneNumber"));
		params.put("ValidCode", checkcode_str);
		params.put("PassWord", pwd_num_str);
		
		VolleyRequest.RequestPost(this, Method.POST, url, tag, params, new VolleyInterface(this, VolleyInterface.mListener, VolleyInterface.mErrorListtener)
		{

			@Override
			public void onMySuccess(String result)
			{
				
				System.out.println("Success**" + result);
				
				JSONObject jsonObject;
				String code="";
				try
				{
					jsonObject = new JSONObject(result);
					code = jsonObject.getString("code");
				} catch (JSONException e)
				{
					e.printStackTrace();
				}
				if (code.equals("1"))
				{
					Toast.makeText(getApplicationContext(), "ע��ɹ�", 0).show();
					finish();
					
				}
				else 
				{
					Toast.makeText(getApplicationContext(), "result="+result, 0).show();
				}
				
				
				
			}

			@Override
			public void onMyError(VolleyError error)
			{
				System.out.println("error**" + error);
				Toast.makeText(getApplicationContext(), "��������ʧ��   ������", 0).show();
			}
		});

	}
	
}
