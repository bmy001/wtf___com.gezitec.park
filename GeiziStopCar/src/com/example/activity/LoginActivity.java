package com.example.activity;

import com.example.aaageizistopcar.R;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends BaseActivity implements OnClickListener
{
	
	EditText et_account;
	EditText et_pwd;
	Button btn_login;
	Button btn_register;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_login);
		initView();
		setListenerEvent();
	}
	
	private void initView()
	{
		et_account=(EditText) findViewById(R.id.et_account);
		et_pwd=(EditText) findViewById(R.id.et_pwd);
		
		btn_login=(Button) findViewById(R.id.btn_login);
		btn_register=(Button) findViewById(R.id.btn_register);
	}
	
	
	private void setListenerEvent()
	{
		// TODO Auto-generated method stub
		btn_login.setOnClickListener(this);
		btn_register.setOnClickListener(this);
	}

	@Override
	public void onClick(View v)
	{
		switch (v.getId())
		{
		case R.id.btn_login:
		
			String account=et_account.getText().toString().trim();
			String pwd=et_pwd.getText().toString().trim();
			
			Intent intent=new Intent(LoginActivity.this, MainActivity.class);
			startActivity(intent);
			
			
			break;
		case R.id.btn_register:
//			Toast.makeText(getApplicationContext(), "R.id.btn_register", 0).show();
			
			Intent intent2=new Intent(LoginActivity.this, RegisterPhoneActivity.class);
			startActivity(intent2);
			
			break;

		default:
			break;
		}
		
	}
}
