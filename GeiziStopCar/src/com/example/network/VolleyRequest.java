package com.example.network;

import java.util.HashMap;
import java.util.Map;

import com.android.volley.AuthFailureError;
import com.android.volley.Request.Method;
import com.android.volley.toolbox.StringRequest;
import com.example.activity.MyAppliction;

import android.content.Context;

public class VolleyRequest
{
	public static StringRequest stringRequest;
	public static Context mContext;

	public static void RequestGet(Context mContext, String url, String tag, VolleyInterface vif)
	{
//		MyAppliction.getHttpQueues().cancelAll(tag);
		
		stringRequest = new StringRequest(Method.GET, url, vif.loadingListener(), vif.errorListener());
		// 设置标签
		stringRequest.setTag(tag);
		// 加入队列
		MyAppliction.getHttpQueues().add(stringRequest);
		// 启动
		MyAppliction.getHttpQueues().start();
	}

	public static void RequestPost(Context mContext, int method, String url, String tag, final Map<String, String> params, VolleyInterface vif)
	{
		MyAppliction.getHttpQueues().cancelAll(tag);
		stringRequest = new StringRequest(Method.POST,url, vif.loadingListener(), vif.errorListener())
		{
			@Override
			protected Map<String, String> getParams() throws AuthFailureError
			{
				// TODO Auto-generated method stub
				return params;
				
			}
		};
		// 设置标签
		stringRequest.setTag(tag);
		// 加入队列
		MyAppliction.getHttpQueues().add(stringRequest);
		// 启动
		MyAppliction.getHttpQueues().start();
	}
}
